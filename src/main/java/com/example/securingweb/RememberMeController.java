package com.example.securingweb;

import com.example.securingweb.service.CustomTokenBasedRememberMeServices;
import com.example.securingweb.service.RememberMeServicesEx;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
public class RememberMeController {

	private final RememberMeServicesEx rememberMeServices;

	public RememberMeController(RememberMeServicesEx rememberMeServices) {
		this.rememberMeServices = rememberMeServices;
	}

	@GetMapping("hoge")
	public String get(HttpServletRequest request, HttpServletResponse response) {
		rememberMeServices.createRememberMeToken(request, response);
		return "hello,world";
	}
}
