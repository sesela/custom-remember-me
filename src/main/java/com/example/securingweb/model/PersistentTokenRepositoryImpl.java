package com.example.securingweb.model;

import org.hibernate.Session;
import org.springframework.security.web.authentication.rememberme.PersistentRememberMeToken;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.Date;

@Transactional
public class PersistentTokenRepositoryImpl implements PersistentTokenRepository {

	private final EntityManager entityManager;

	public PersistentTokenRepositoryImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Override
	public void createNewToken(PersistentRememberMeToken token) {
		PersistentLogin login = new PersistentLogin();
		login.setUsername(token.getUsername());
		login.setSeries(token.getSeries());
		login.setToken(token.getTokenValue());
		login.setLastUsed(token.getDate());
		Session session = entityManager.unwrap(Session.class);
		session.save(login);
		}

	@Override
	public PersistentRememberMeToken getTokenForSeries(String seriesId) {
		Session session = entityManager.unwrap(Session.class);
		PersistentLogin login = session
				.get(PersistentLogin.class, seriesId);

		if (login != null) {
			return new PersistentRememberMeToken(login.getUsername(),
					login.getSeries(), login.getToken(),login.getLastUsed());
		}

		return null;
	}

	@Override
	public void removeUserTokens(String username) {
		Session session = entityManager.unwrap(Session.class);
		session.createQuery("delete from PersistentLogin"
				+ " where username=:userName")
				.setParameter("userName", username).executeUpdate();
	}

	@Override
	public void updateToken(String series, String tokenValue, Date lastUsed) {
		Session session = entityManager.unwrap(Session.class);
		PersistentLogin login =session.get(PersistentLogin.class, series);
		login.setToken(tokenValue);
		login.setLastUsed(lastUsed);
		session.save(login);
	}

}