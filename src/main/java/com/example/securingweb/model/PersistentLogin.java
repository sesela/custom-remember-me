package com.example.securingweb.model;


import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Entity
@Data
public class PersistentLogin {

	@Id
	@Column
	private String series;

	@Column(nullable = false)
	private String username;

	@Column(nullable = false)
	private String token;

	@Column(nullable = false)
	private Date lastUsed;

	//Setter and Getter methods
}