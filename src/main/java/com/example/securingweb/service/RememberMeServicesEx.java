package com.example.securingweb.service;

import org.springframework.security.web.authentication.RememberMeServices;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface RememberMeServicesEx extends RememberMeServices {

	void createRememberMeToken(HttpServletRequest request, HttpServletResponse response);
	String getKey();
}
