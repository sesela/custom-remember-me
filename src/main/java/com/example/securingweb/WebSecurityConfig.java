package com.example.securingweb;

import com.example.securingweb.model.PersistentTokenRepositoryImpl;
import com.example.securingweb.service.CustomPersistentTokenBasedRememberMeServices;
import com.example.securingweb.service.CustomTokenBasedRememberMeServices;
import com.example.securingweb.service.RememberMeServicesEx;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.ObjectPostProcessor;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.authentication.rememberme.AbstractRememberMeServices;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.security.web.authentication.rememberme.RememberMeAuthenticationFilter;

import javax.persistence.EntityManager;

@Configuration
@EnableWebSecurity(debug = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	private final EntityManager entityManager;

	public WebSecurityConfig(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/css/**", "/fonts/**", "/images/**", "/js/**", "/h2-console/**");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.authorizeRequests()
				.antMatchers("/", "/home").permitAll()
				.anyRequest().authenticated()
				.and()
			.formLogin()
				.loginPage("/login")
				.permitAll()
				.and()
			.logout()
				.permitAll();
		http.rememberMe()
				.key(getRememberMeService().getKey())
				.rememberMeServices(getRememberMeService())
				.withObjectPostProcessor(new ObjectPostProcessor<RememberMeAuthenticationFilter>() {
			@Override
			public <O extends RememberMeAuthenticationFilter> O postProcess( O object) {
				AbstractRememberMeServices rms = (AbstractRememberMeServices) object.getRememberMeServices();
				rms.setAlwaysRemember(false);
				return object;
			}
		});
	}


	@Bean
	@Override
	public UserDetailsService userDetailsService() {
		UserDetails user = User.withUsername("user")
				.password(passwordEncoder().encode("password"))
				.roles("USER")
				.build();
		return new InMemoryUserDetailsManager(user);
	}

	@Bean
	RememberMeServicesEx getRememberMeService() {
		// DB
		return new CustomPersistentTokenBasedRememberMeServices("KEY2", userDetailsService(), persistentTokenRepository());
//		// InMemory
//		return new CustomTokenBasedRememberMeServices("KEY1", userDetailsService());
	}

	@Bean
	PersistentTokenRepository persistentTokenRepository() {
		PersistentTokenRepositoryImpl tokenRepositoryImpl = new PersistentTokenRepositoryImpl(entityManager);
		return tokenRepositoryImpl;
	}

	@Bean
	PasswordEncoder passwordEncoder() {
		return PasswordEncoderFactories.createDelegatingPasswordEncoder();
	}

}
